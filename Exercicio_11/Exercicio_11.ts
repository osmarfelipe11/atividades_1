/*Faça um programa que receba um número positivo e
maior que zero, calcule e mostre:
a) O número digitado ao quadrado;
b) O número digitado ao cubo;
c) A raiz quadrada do número digitado;
d) A raiz cúbica de número digitado. */
//inicio
namespace exercicio_11 
{
    //entrada
    const numero = 10;
    
    let numQ: number;

    numQ = numero ** 2;

    let numC: number;

    numC = numero ** 3;

    let raizQ: number;

    raizQ = Math.sqrt(numero);

    let raizC: number;

    raizC = Math.cbrt(numero);

        //saida
    console.log(`Ao quadrado: \n ${numQ} \n Ao cubo: \n ${numC} \n Raiz quadrada: \n ${raizQ} \n Raiz cubica: \n ${raizC}`)
}