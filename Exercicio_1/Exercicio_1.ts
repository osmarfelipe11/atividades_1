/* Faça um programa que receba quatro números inteiros,
calcule e mostre a soma desses números. */

//primeiro bloco
// inicício
namespace exercicio_1 
{
    //Entrada de dados
    // var - let - const
    let num1, num2, num3, num4: number;

    num1 = 1;
    num2 = 3;
    num3 = 6;
    num4 = 7;

    let soma: number;
    
    //processar dados
    soma = num1 + num2 + num3 + num4;
    //saída de dados
    console.log(`A soma de ${num1}, ${num2}, ${num3} e ${num4} é: \n ${soma}`);
}
